<?php
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @file
 */

namespace MediaWiki\Extension\AutoModerator;

use CommentStoreComment;
use FormatJson;
use MediaWiki\Logger\LoggerFactory;
use MediaWiki\MediaWikiServices;
use MediaWiki\Page\Hook\RevisionFromEditCompleteHook;
use MediaWiki\Page\WikiPage;
use MediaWiki\Revision\RevisionRecord;
use MediaWiki\Revision\SlotRecord;

class Hooks implements
	RevisionFromEditCompleteHook
	{
		/**
		 * Returns the result of a three-way merge when undoing changes.
		 *
		 * @param WikiPage $wikiPage WikiPage edited
		 * @param RevisionRecord $undoRev Newest revision being undone. Corresponds to `undo`
		 *        URL parameter.
		 * @param RevisionRecord $oldRev Revision that is being restored. Corresponds to
		 *        `undoafter` URL parameter.
		 * @param ?string &$error If false is returned, this will be set to "norev"
		 *   if the revision failed to load, or "failure" if the content handler
		 *   failed to merge the required changes.
		 *
		 * @return Content|false
		 */
		private function getUndoContent(
			\WikiPage $wikiPage,
			\MediaWiki\Revision\RevisionStoreRecord $undoRev,
			\MediaWiki\Revision\RevisionStoreRecord $oldRev,
			&$error
		) {
			$handler = MediaWikiServices::getInstance()
				->getContentHandlerFactory()
				->getContentHandler( $undoRev->getSlot(
					SlotRecord::MAIN,
					RevisionRecord::RAW
				)->getModel() );
			$currentContent = $wikiPage->getRevisionRecord()
				->getContent( SlotRecord::MAIN );
			$undoContent = $undoRev->getContent( SlotRecord::MAIN );
			$undoAfterContent = $oldRev->getContent( SlotRecord::MAIN );
			$undoIsLatest = $wikiPage->getRevisionRecord()->getId() === $undoRev->getId();
			if ( $currentContent === null
				|| $undoContent === null
				|| $undoAfterContent === null
			) {
				$error = 'norev';
				return false;
			}

			$content = $handler->getUndoContent(
				$currentContent,
				$undoContent,
				$undoAfterContent,
				$undoIsLatest
			);
			if ( $content === false ) {
				$error = 'failure';
			}
			return $content;
		}

	/**
	 * Make a single call to LW revert risk model for one revid and return the decoded result.
	 *
	 * @param string $revid
	 *
	 * @return array Decoded response
	 */
	public function singleLiftWingRequest( $revid ) {
		$url = 'https://api.wikimedia.org/service/lw/inference/v1/models/revertrisk-language-agnostic:predict';
		$language = 'en';
		$logger = LoggerFactory::getInstance( 'AutoModerator' );
		$logger->debug( "AutoModerator Requesting: {$url} " . __METHOD__ );
		$req = MediaWikiServices::getInstance()
			->getHttpRequestFactory()->create( $url, [
				'method' => 'POST',
				'postData' => json_encode( [
					'rev_id' => (int)$revid,
					'lang' => $language,
				] ),
			] );
		$status = $req->execute();
		if ( !$status->isOK() ) {
			$message = "Failed to make LiftWing request to [{$url}], " .
				Status::wrap( $status )->getMessage()->inLanguage( 'en' )->text();
			// Server time out, try again once
			if ( $req->getStatus() === 504 ) {
				$req = $this->httpRequestFactory->create( $url, [
					'method' => 'POST',
					'postData' => json_encode( [ 'rev_id' => (int)$revid ] ),
				],
				);
				$status = $req->execute();
				if ( !$status->isOK() ) {
					throw new RuntimeException( $message );
				}
			} elseif ( $req->getStatus() === 400 ) {
				$logger->debug( "400 Bad Request: {$message} " . __METHOD__ );
				$data = FormatJson::decode( $req->getContent(), true );
				if ( strpos( $data["error"], "The MW API does not have any info related to the rev-id" ) === 0 ) {
					return $this->createRevisionNotFoundResponse( $model, $revid );
				} else {
					throw new RuntimeException( $message );
				}
			} else {
				throw new RuntimeException( $message );
			}
		}
		$json = $req->getContent();
		$logger->debug( "Raw response: {$json} " . __METHOD__ );
		$data = FormatJson::decode( $json, true );
		if ( !$data || !empty( $data['error'] ) ) {
			throw new RuntimeException( "Bad response from Lift Wing endpoint [{$url}]: {$json}" );
		}
		return $data;
	}

	/**
	 * @see https://www.mediawiki.org/wiki/Manual:Hooks/RevisionFromEditComplete
	 *
	 * @param \WikiPage $wikiPage WikiPage edited
	 * @param RevisionRecord $rev New revision
	 * @param int|false $originalRevId If the edit restores or repeats an earlier revision (such as a
	 *   rollback or a null revision), the ID of that earlier revision. False otherwise.
	 *   (Used to be called $baseID.)
	 * @param UserIdentity $user Editing user
	 * @param string[] &$tags Tags to apply to the edit and recent change. This is empty, and
	 *   replacement is ignored, in the case of import or page move.
	 *
	 * @return bool|void True or no return value to continue or false to abort
	 */
	public function onRevisionFromEditComplete( $wikiPage, $rev, $originalRevId,
		$user, &$tags ) {
		// Don't revert reverts or null
		if ( $originalRevId ) {
			return;
		}
		$revisionLookup = MediaWikiServices::getInstance()->getRevisionLookup();
		$score = $this->singleLiftWingRequest( $rev->getId() );
		$probability = $score[ 'output' ][ 'probabilities' ][ 'true' ];
		if ( $probability && $probability > 0.2 ) {
			$prevRev = $revisionLookup->getPreviousRevision( $rev );
			$pageUpdater = $wikiPage->newPageUpdater( $user );
			$undoMsg = 'AutoModerator Revert with probability ' . $probability . '.';
			$content = $this->getUndoContent( $wikiPage, $rev, $prevRev, $undoMsg );
			if ( !$content ) {
				return;
			}
			$pageUpdater->setContent( SlotRecord::MAIN, $content );
			$comment = CommentStoreComment::newUnsavedComment( $undoMsg );
			// REVERT_MANUAL = 3
			$pageUpdater->markAsRevert( 3, $rev->getId(), $prevRev->getId() );
			// EDIT_UPDATE = 2
			$pageUpdater->saveRevision( $comment, 2 );
		}
	}
}
