<?php

namespace MediaWiki\Extension\AutoModerator\Tests;

use MediaWiki\Extension\AutoModerator\Hooks;

/**
 * @coversDefaultClass \MediaWiki\Extension\AutoModerator\Hooks
 */
class HooksTest extends \MediaWikiUnitTestCase {

	/**
	 * @covers ::onRevisionFromEditComplete
	 */
	public function testOnRevisionFromEditCompleteVandalizeIsTrue() {
		$config = new \HashConfig( [
			'AutoModeratorVandalizeEachPage' => true
		] );
		$outputPageMock = $this->getMockBuilder( \OutputPage::class )
			->disableOriginalConstructor()
			->getMock();
		$outputPageMock->method( 'getConfig' )
			->willReturn( $config );

		$outputPageMock->expects( $this->once() )
			->method( 'addHTML' )
			->with( '<p>AutoModerator was here</p>' );
		$outputPageMock->expects( $this->once() )
			->method( 'addModules' )
			->with( 'oojs-ui-core' );

		$skinMock = $this->getMockBuilder( \Skin::class )
			->disableOriginalConstructor()
			->getMock();

		( new Hooks )->onRevisionFromEditComplete( $outputPageMock, $skinMock );
	}

	/**
	 * @covers ::onRevisionFromEditComplete
	 */
	public function testOnRevisionFromEditCompleteVandalizeFalse() {
		$config = new \HashConfig( [
			'AutoModeratorVandalizeEachPage' => false
		] );
		$outputPageMock = $this->getMockBuilder( \OutputPage::class )
			->disableOriginalConstructor()
			->getMock();
		$outputPageMock->method( 'getConfig' )
			->willReturn( $config );
		$outputPageMock->expects( $this->never() )
			->method( 'addHTML' );
		$outputPageMock->expects( $this->never() )
			->method( 'addModules' );
		$skinMock = $this->getMockBuilder( \Skin::class )
			->disableOriginalConstructor()
			->getMock();
		( new Hooks )->onRevisionFromEditComplete( $outputPageMock, $skinMock );
	}

}
